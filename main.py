#Input: digits = [4,3,2,1]
#Output: [4,3,2,2]

def plusone(digits):
    num=0
    tens=10**(len(digits)-1)

    for digit in digits:
        num=tens*digit+num
        tens=tens//10
    num=num+1
    return [int(number) for number in str(num)]
plusone([1,9])
#this isnot  a new file